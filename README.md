# Atheneum [![pipeline status](https://gitlab.com/WarrickSothr/Atheneum/badges/master/pipeline.svg)](https://gitlab.com/WarrickSothr/Atheneum/commits/master)

An ebook/comic library service and web client.

## Parts

### Server

The core API server

More information available at server/README.md

### Client

The primary SPA frontend

More information available at client/README.md

### Administration

The administration SPA.

More information available at administration/README.md

## Release History

## Changelog

See:
* server/CHANGELOG.md
* client/CHANGELOG.md
* administration/CHANGELOG.md

## FAQ

* TODO

## Maintainers

* Drew Short <warrick(AT)sothr(DOT)com>