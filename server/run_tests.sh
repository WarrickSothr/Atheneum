#!/usr/bin/env bash

set -e
set -x


python3 --version
python3 -m pip --version

pylint --version
mypy --version
coverage --version
pytest --version
pycodestyle --version
pydocstyle --version

pylint atheneum
mypy atheneum tests
PYTHONPATH=$(pwd) coverage run --source atheneum -m pytest
coverage report --fail-under=85 -m --skip-covered
pycodestyle atheneum tests
pydocstyle atheneum
