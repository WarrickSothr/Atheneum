"""Authentication specific utilities."""
from typing import Tuple

from nacl import pwhash


def get_password_hash(password: str) -> Tuple[str, int]:
    """
    Retrieve argon2id password hash.

    :param password: plaintext password to convert
    :return: Tuple[password_hash, password_revision]
    """
    return pwhash.argon2id.str(password.encode('utf8')).decode('utf8'), 1
