"""Authentication API blueprint and endpoint definitions."""
from flask import Blueprint, g

from atheneum.api.decorators import return_json
from atheneum.api.model import APIMessage, APIResponse
from atheneum.middleware import authentication_middleware
from atheneum.service import (
    user_token_service,
    authentication_service,
    user_service
)

AUTH_BLUEPRINT = Blueprint(
    name='auth', import_name=__name__, url_prefix='/auth')


@AUTH_BLUEPRINT.route('/login', methods=['POST'])
@return_json
@authentication_middleware.require_basic_auth
def login() -> APIResponse:
    """
    Get a token for continued authentication.

    :return: A login token for continued authentication
    """
    user_token = user_token_service.create(g.user)
    return APIResponse(user_token, 200)


@AUTH_BLUEPRINT.route('/bump', methods=['POST'])
@return_json
@authentication_middleware.require_token_auth
def login_bump() -> APIResponse:
    """
    Update the user last seen timestamp.

    :return: A time stamp for the bumped login
    """
    user_service.update_last_login_time(g.user)
    return APIResponse(g.user, 200, ['lastLoginTime'])


@AUTH_BLUEPRINT.route('/logout', methods=['POST'])
@return_json
@authentication_middleware.require_token_auth
def logout() -> APIResponse:
    """
    Logout and delete a token.

    :return:
    """
    authentication_service.logout(g.user_token)
    return APIResponse(APIMessage(True, None), 200)
