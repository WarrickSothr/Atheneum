"""Database configuration and methods."""
from flask_migrate import upgrade
from flask_sqlalchemy import SQLAlchemy

db: SQLAlchemy = SQLAlchemy()


def init_db() -> None:
    """Clear existing data and create new tables."""
    upgrade('migrations')
