from setuptools import setup

setup(
    name='atheneum',
    packages=['atheneum'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)