from tests.conftest import AuthActions


def test_login_happy_path(auth: AuthActions):
    result = auth.login()
    assert result.status_code == 200
    assert result.json['token'] is not None and len(result.json['token']) > 0


def test_bump_happy_path(auth: AuthActions):
    auth.login()
    result = auth.bump()
    assert result.status_code == 200
    assert (result.json['lastLoginTime'] is not None
            and len(result.json['lastLoginTime']) > 0)


def test_logout_happy_path(auth: AuthActions):
    auth.login()
    result = auth.logout()
    assert result.status_code == 200
    assert result.json['success']
