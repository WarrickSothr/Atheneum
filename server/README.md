# Atheneum Server

## [API Documentation](https://warricksothr.gitlab.io/Atheneum)

## Requirements

* Python 3.6
* Pipenv

## Installation

```bash
git clone https://gitlab.com/WarrickSothr/Atheneum.git
cd Atheneum/server
pipenv install
pipenv shell
```

## Configuration

## Running

### Docker

```bash
docker build -t atheneum:local-test .
docker run -d atheneum:local-test
```

### Local Development Version

```bash
FLASK_APP=atheneum:atheneum flask db upgrade
python manage.py user register-admin
FLASK_APP=atheneum:atheneum flask run
```

## FAQ

## Development

```bash
pipenv install --dev
```

* Make changes
* Add/Update tests

```bash
./run_tests
```

* If everything passes follow contributing guide.

## Contributing

See ../CONTRIBUTING.md