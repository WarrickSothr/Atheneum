"""Initial User DB Migration
Revision ID: 96442b147e22
Revises: 
Create Date: 2018-07-03 14:22:42.833390

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '96442b147e22'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('user',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.Unicode(length=60), nullable=False),
                    sa.Column('role', sa.Unicode(length=32), nullable=False),
                    sa.Column(
                        'password_hash',
                        sa.Unicode(length=128),
                        nullable=False),
                    sa.Column(
                        'password_revision', sa.SmallInteger(), nullable=False),
                    sa.Column('creation_time', sa.DateTime(), nullable=False),
                    sa.Column('last_login_time', sa.DateTime(), nullable=True),
                    sa.Column('version', sa.Integer(), nullable=False),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('name'))

    op.create_table('user_token',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('token', sa.Unicode(length=36), nullable=False),
                    sa.Column('note', sa.Unicode(length=128), nullable=True),
                    sa.Column('enabled', sa.Boolean(), nullable=False),
                    sa.Column('expiration_time', sa.DateTime(), nullable=True),
                    sa.Column('creation_time', sa.DateTime(), nullable=False),
                    sa.Column('last_edit_time', sa.DateTime(), nullable=True),
                    sa.Column('last_usage_time', sa.DateTime(), nullable=True),
                    sa.Column('version', sa.Integer(), nullable=False),
                    sa.ForeignKeyConstraint(
                        ['user_id'], ['user.id'], ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('id'))

    op.create_index(op.f('ix_user_token_user_id'), 'user_token', ['user_id'],
                    unique=False)


def downgrade():
    op.drop_index(op.f('ix_user_token_user_id'), table_name='user_token')
    op.drop_table('user_token')
    op.drop_table('user')
